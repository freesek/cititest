package ultra.pro.alexciti.ui.first

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import ultra.pro.alexciti.R
import ultra.pro.alexciti.ui.first.utils.SimpleUndoableText
import ultra.pro.alexciti.ui.first.utils.UpperUndoableText

class FirstSolutionFragment : Fragment() {

    private lateinit var result : TextView
    private lateinit var enterButton : AppCompatButton
    private lateinit var eraseButton : AppCompatButton
    private lateinit var setCursorButton : AppCompatButton
    private lateinit var cursorPosition : TextView
    private lateinit var undoButton : AppCompatButton
    private lateinit var redoButton : AppCompatButton
    private lateinit var text : SimpleUndoableText
    private lateinit var enterText : AppCompatEditText
    private lateinit var enterIndex : AppCompatEditText
    private lateinit var switchText : Switch

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root  = inflater.inflate(R.layout.fragment_home, container, false)
        text = SimpleUndoableText()
        text.confListener = { line, index ->
            result.text = line
            cursorPosition.text = "$index"
        }
        result = root.findViewById(R.id.result)
        enterButton = root.findViewById(R.id.enter)
        eraseButton = root.findViewById(R.id.erase)
        setCursorButton = root.findViewById(R.id.set_cursor)
        cursorPosition = root.findViewById(R.id.cursor_value)
        undoButton = root.findViewById(R.id.undo)
        redoButton = root.findViewById(R.id.redo)
        enterText = root.findViewById(R.id.enter_text)
        enterIndex = root.findViewById(R.id.set_cursor_text)
        switchText = root.findViewById(R.id.upper_switch)
        clickListeners()
        return root
    }

    fun clickListeners() {
        enterButton.setOnClickListener{
            if(enterText.text.toString().isNotEmpty()) {
                text.enter(enterText.text.toString()[0])
                enterText.setText("")
            }

        }
        eraseButton.setOnClickListener{
            text.erase()
        }
        setCursorButton.setOnClickListener{
            val cursorPos = enterIndex.text.toString().toIntOrNull()
            if(cursorPos != null && cursorPos >= 0) {
                text.setCursor(cursorPos)
                enterIndex.setText("")
            } else {
                Toast.makeText(activity, "Курсор должен быть положительным числом", Toast.LENGTH_SHORT).show()
            }
        }
        undoButton.setOnClickListener{
            text.undo()
        }
        redoButton.setOnClickListener{
            text.redo()
        }
        switchText.setOnCheckedChangeListener{ _,state ->
            if(!state) {
                text = SimpleUndoableText()
            } else {
                text = UpperUndoableText()
            }
            text.confListener = { line, index ->
                result.text = line
                cursorPosition.text = "$index"
            }
        }
    }
}