package ultra.pro.alexciti.ui.first.utils

import ultra.pro.alexciti.utils.UndoableText

class UpperUndoableText : SimpleUndoableText() {

    override fun enter(symb: Char) {
        super.enter(symb.toUpperCase())
    }

}