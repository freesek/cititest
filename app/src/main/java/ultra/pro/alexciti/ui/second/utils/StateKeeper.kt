package ultra.pro.alexciti.ui.second.utils

import ultra.pro.alexciti.utils.Text

class StateKeeper {

    val stateList : ArrayList<Text> = ArrayList()

    companion object {
        private const val MAX_STATE_COUNT : Int = 10
    }
}