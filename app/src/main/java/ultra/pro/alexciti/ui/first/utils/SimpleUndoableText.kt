package ultra.pro.alexciti.ui.first.utils

import ultra.pro.alexciti.utils.UndoableText

open class SimpleUndoableText : UndoableText {

    override val size: Int
        get() = list.size
    override var curIndex: Int = 0

    private val cmdList = ArrayList<CommandItem>()
    private var cmdListIndex = 0

    var confListener: ((String, Int) -> Unit)? = null

    private val list = ArrayList<Char>()

    override fun toString(): String =
        list.joinToString("")

    override fun undo() {
        sendCommand(Command.UNDO)
    }

    override fun redo() {
        sendCommand(Command.REDO)
    }

    override fun enter(symb: Char) {
        sendCommand(Command.ENTER, symb)
    }

    override fun erase() {
        sendCommand(Command.ERASE)
    }

    override fun setCursor(index: Int) {
        sendCommand(Command.SET_CURSOR, index)
    }

    override fun iterator(): Iterator<Char> = object : Iterator<Char> {
        override fun hasNext(): Boolean =
            curIndex == size - 1

        override fun next(): Char = list[++curIndex]
    }

    private fun sendCommand(cmd: Command, value: Any? = null) {
        when (cmd) {
            Command.ENTER -> {
                cmdList.addCommand(
                    CommandItem(
                        command = Command.ENTER,
                        symbol = value as Char,
                        indexChange = 1
                    )
                )
                list.add(curIndex++, value as Char)
            }
            Command.ERASE -> {
                if (curIndex > 0) {
                    cmdList.addCommand(CommandItem(Command.ERASE, list[curIndex - 1], -1))
                    list.removeAt(--curIndex)
                }
            }
            Command.SET_CURSOR -> {
                val index = value as Int
                val changeCursor = curIndex - if (index < size)
                    index
                else size
                cmdList.addCommand(
                    CommandItem(
                        command = Command.SET_CURSOR,
                        indexChange = changeCursor
                    )
                )
                curIndex = if (index < size)
                    index
                else size
            }
            Command.REDO -> {
                if (cmdListIndex != cmdList.size) {
                    when (cmdList[cmdListIndex].command) {
                        Command.ENTER -> {
                            list.add(curIndex++, cmdList[cmdListIndex].symbol!!)
                        }
                        Command.ERASE -> {
                            list.removeAt(--curIndex)
                        }
                        Command.SET_CURSOR -> {
                            curIndex = if (curIndex - cmdList[cmdListIndex].indexChange < size)
                                curIndex - cmdList[cmdListIndex].indexChange
                            else size
                        }
                        else -> {
                        }
                    }
                    cmdListIndex++
                }
            }
            Command.UNDO -> {
                if (cmdListIndex != 0) {
                    when (cmdList[cmdListIndex - 1].command) {
                        Command.ENTER -> {
                            list.removeAt(--curIndex)
                        }
                        Command.ERASE -> {
                            cmdList[cmdListIndex - 1].symbol?.let {
                                if (curIndex == list.size) {
                                    list.add(it)
                                    curIndex++
                                } else {
                                    list.add(curIndex++, it)
                                }
                            }
                        }
                        Command.SET_CURSOR -> {
                            curIndex += cmdList[cmdListIndex - 1].indexChange
                        }
                        else -> {
                        }
                    }
                    cmdListIndex--
                }
            }
        }
        confListener?.invoke(toString(), curIndex)
    }

    enum class Command {
        ENTER, ERASE, SET_CURSOR, UNDO, REDO
    }

    data class CommandItem(
        val command: Command,
        val symbol: Char? = null,
        val indexChange: Int
    )

    private fun ArrayList<CommandItem>.addCommand(item: CommandItem) {
        subList(cmdListIndex++, size).clear()
        add(item)
    }


}