package ultra.pro.alexciti.utils

enum class CommandType() {
    ENTER,ERASE,UNDO,REDO,CURSOR
}