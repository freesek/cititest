package ultra.pro.alexciti.utils

interface Text : Iterable<Char>{
    val size : Int
    var curIndex : Int
    fun enter(symb : Char)
    fun erase()
    fun setCursor(index : Int)
}