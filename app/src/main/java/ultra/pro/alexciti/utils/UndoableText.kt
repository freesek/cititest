package ultra.pro.alexciti.utils

interface UndoableText : Text {
    fun undo()
    fun redo()
}